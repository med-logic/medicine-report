
from django.apps import AppConfig
from django.conf import settings


class ReportConfig(AppConfig):

    name = 'medicine_report'
    verbose_name = 'Отчеты'

    def ready(self):
        import medicine_report.recievers
