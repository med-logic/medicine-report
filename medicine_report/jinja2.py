
import re

from jinja2 import Environment, TemplateError, TemplateSyntaxError, UndefinedError


TemplateError = TemplateError
TemplateSyntaxError = TemplateSyntaxError

jinja_env = Environment()
