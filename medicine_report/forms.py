
from django import forms

EMPTY_VALUES = ['', 'None', None]


class ReportFilterForm(forms.Form):

    def get_errors_display(self):
        errors = []

        for field_name, error in self.errors.items():
            errors.append(f'{self.fields[field_name].label} - {error[0]}')

        return ' '.join(errors)

    def get_filter_display(self):
        data = self.cleaned_data
        values = []
        for field_name in self.fields:
            if field_name in data:
                if data[field_name] not in EMPTY_VALUES:
                    display = self[field_name].field.get_value_display(data[field_name])
                    if self.fields[field_name].label:
                        label = self.fields[field_name].label.lower()
                        values.append(f'{label}: {display}')

        return ', '.join(values)
