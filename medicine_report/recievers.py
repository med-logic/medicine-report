
from django.db.models.signals import post_save, post_delete, pre_delete, m2m_changed
from django.dispatch import receiver

from .models import *

__all__ = ['report_function_post_delete']


@receiver(post_delete, sender=ReportFunction)
def report_function_post_delete(instance, *args, **kwargs):
    ReportFunction.drop_function(instance.sql)


@receiver(post_delete, sender=ReportView)
def report_function_post_delete(instance, *args, **kwargs):
    ReportView.drop_view(instance.name, instance.is_materialized)
