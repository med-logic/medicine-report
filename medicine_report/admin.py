
import re

from django import forms
from django.contrib import admin
from django.contrib.admin.widgets import FilteredSelectMultiple
from django.contrib.contenttypes.models import ContentType
from django.urls import path, reverse
from django.http import HttpResponseRedirect
from django.core.exceptions import FieldError
from django.conf import settings
from django.db import models

from import_export.admin import ImportExportActionModelAdmin
from import_export.formats import base_formats

from medicine_utils.form.widgets import AceEditorWidget

from .import_export import ReportResource, ReportTemplateVariableResource
from .models import *
from .jinja2 import jinja_env, TemplateSyntaxError
from . import tasks


@admin.register(ReportCategory)
class ReportCategoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'is_internal', 'is_active',)
    list_editable = ('is_active',)
    list_filter = ('is_active', 'is_internal',)
    form = forms.modelform_factory(ReportCategory, fields='__all__', widgets={
        'permission_groups': FilteredSelectMultiple(verbose_name='Группы', is_stacked=False),
        'permission_users': FilteredSelectMultiple(verbose_name='Пользователи', is_stacked=False),
    })


class ReportAdminForm(forms.ModelForm):

    class Meta:
        model = Report
        fields = '__all__'
        widgets = {
            'description': forms.Textarea(attrs={'rows': 4}),
            'template_html': AceEditorWidget(mode='django'),
        }

    def clean_template_html(self):
        template_html = self.cleaned_data.get('template_html', None)
        if template_html and len(template_html) > 0:
            try:
                jinja_env.parse(template_html)
            except TemplateSyntaxError as e:
                raise forms.ValidationError(f'Line {e.lineno} - {e.__str__()}')
            except Exception as e:
                raise forms.ValidationError(e)

        return template_html

    def clean_is_internal(self):
        is_internal = self.cleaned_data.get('is_internal')
        if is_internal is True and settings.DEBUG is False:
            raise forms.ValidationError('Данный отчет не может быть системным')

        return is_internal


class ReportQueryInline(admin.StackedInline):
    model = ReportQuery
    extra = 0
    fieldsets = (('', {
        'fields': (('name', 'title'), 'sql')
    }),)


class ReportQueryField(admin.TabularInline):
    model = ReportQueryField
    extra = 0
    form = forms.modelform_factory(ReportQueryField, fields='__all__', widgets={
        'name': forms.TextInput(attrs={'size': 15}),
        'label': forms.TextInput(attrs={'size': 20}),
        'default_value': forms.TextInput(attrs={'size': 15}),
        'test_value': forms.TextInput(attrs={'size': 15}),
    })


@admin.register(Report)
class ReportAdmin(ImportExportActionModelAdmin):
    list_display = ('id', 'name', 'category', 'is_internal', 'is_active',)
    list_display_links = ('id', 'name')
    list_editable = ('is_active',)
    list_filter = ('is_active', 'is_internal', 'is_async', 'category',)
    search_fields = ('id', 'name',)
    form = ReportAdminForm
    change_list_template = 'admin/report/report/change_list.html'
    resource_class = ReportResource
    formats = [base_formats.JSON]

    fieldsets = (
        ('', {
            'fields': (('name', 'category', 'is_active', 'is_internal', 'is_async',),
                       ('result_per_page', 'template_name', 'orientation', 'generate_spreadsheet', 'generate_pdf'),)
        }),
        ('HTML шаблон', {
            'fields': ('template_html',)
        }),
        ('Дополнительно', {
            'classes': ('collapse',),
            'fields': ('description',)
        }),
    )
    inlines = [ReportQueryInline, ReportQueryField]

    def get_urls(self):
        urls = super().get_urls()
        info = self.get_model_info()
        my_urls = [
            path(r'^sync/$',
                self.admin_site.admin_view(self.sync),
                name='%s_%s_sync' % info),
            path(r'^clear_cache/$',
                self.admin_site.admin_view(self.clear_cache),
                name='%s_%s_clear_cache' % info),
        ]
        return my_urls + urls

    def sync(self, request, *args, **kwargs):
        tasks.import_reports.apply_async(
            queue=settings.CELERY_LONG_QUEUE,
            routing_key=settings.CELERY_LONG_QUEUE
        )
        info = self.get_model_info()
        self.message_user(request, 'Задача на синхронизацию отправлена')
        return HttpResponseRedirect(reverse('admin:%s_%s_changelist' % info))

    def clear_cache(self, request, *args, **kwargs):
        info = self.get_model_info()
        Report.clear_cache()
        self.message_user(request, 'Кеш очищен')
        return HttpResponseRedirect(reverse('admin:%s_%s_changelist' % info))


class ReportCatalogValue(admin.TabularInline):
    model = ReportCatalogValue
    extra = 0


class ReportCatalogForm(forms.ModelForm):

    class Meta:
        model = ReportCatalog
        fields = '__all__'
        widgets = {
            'content_type_filter': AceEditorWidget(mode='json')
        }

    content_type = forms.ModelChoiceField(queryset=ReportCatalog.get_content_type_queryset(), required=False,
                                          label='Справочник')

    def clean(self):
        data = self.cleaned_data
        content_type = data.get('content_type', None)
        content_type_filter = data.get('content_type_filter', None)

        if content_type and content_type_filter:
            try:
                ReportCatalog(content_type=content_type, content_type_filter=content_type_filter)\
                    .get_choices_queryset()
            except (TypeError, FieldError) as e:
                raise forms.ValidationError({'content_type_filter': e})

        return data


@admin.register(ReportCatalog)
class ReportCatalogAdmin(admin.ModelAdmin):
    form = ReportCatalogForm
    inlines = [ReportCatalogValue]


class ReportFunctionAdminForm(forms.ModelForm):

    class Meta:
        model = ReportFunction
        fields = '__all__'
        widgets = {
            'description': forms.Textarea(attrs={'cols': 50, 'rows': 5})
        }

    sql = forms.CharField(widget=AceEditorWidget(mode='sql'), label='')

    def clean(self):
        data = self.cleaned_data

        try:
            ReportFunction.create_or_update_function(data['sql'], self.instance.sql if self.instance else None)
        except Exception as e:
            raise forms.ValidationError(e)

        return data


@admin.register(ReportFunction)
class ReportFunctionAdmin(admin.ModelAdmin):
    list_display = ('name', 'description')
    form = ReportFunctionAdminForm


class ReportViewAdminForm(forms.ModelForm):

    class Meta:
        model = ReportFunction
        fields = '__all__'
        widgets = {
            'description': forms.Textarea(attrs={'cols': 50, 'rows': 5})
        }

    sql = forms.CharField(widget=AceEditorWidget(mode='sql'), initial='SELECT', label='')

    def clean(self):
        data = self.cleaned_data
        try:
            ReportView.create_or_update_view(data['sql'], data['name'], data['is_materialized'])
        except Exception as e:
            raise forms.ValidationError(e)

        return data


@admin.register(ReportView)
class ReportViewAdmin(admin.ModelAdmin):
    list_display = ('name', 'description', 'is_materialized', 'position')
    list_editable = ('position',)
    form = ReportViewAdminForm
    actions = ['update_materialized_views']

    def update_materialized_views(self, request, queryset):
        queryset = queryset.filter(is_materialized=True)
        ids = [view.id for view in queryset]
        if len(ids) > 0:
            tasks.update_materialized_views.apply_async(args=[ids])
            self.message_user(request, 'Задача поставлена на выполение')

    update_materialized_views.short_description = 'Обновить представления'


@admin.register(ReportTemplateVariable)
class ReportTemplateVariableAdmin(ImportExportActionModelAdmin):
    list_display = ('name', 'value',)
    search_fields = ('name', 'value')
    list_editable = ('value',)
    resource_class = ReportTemplateVariableResource
    formats = [base_formats.JSON]
    formfield_overrides = {
        models.TextField: {'widget': forms.Textarea(attrs={'rows': 2})},
    }

    def has_change_permission(self, request, obj=None):
        return request.user.has_perm('report.change_report')

    def has_add_permission(self, request):
        return request.user.has_perm('report.add_report')

    def has_delete_permission(self, request, obj=None):
        return request.user.has_perm('report.delete_report')


@admin.register(ReportResult)
class ReportResultAdmin(admin.ModelAdmin):
    list_filter = ('status',)
    list_display = ('user', 'report', 'created')

    def get_readonly_fields(self, request, obj=None):
        return list(set(
            [field.name for field in self.opts.local_fields] +
            [field.name for field in self.opts.local_many_to_many]
        ))
