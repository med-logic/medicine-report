
from django.core.cache import cache
from django.conf import settings
from rest_framework import serializers

from .models import *


class ReportCategorySerializer(serializers.ModelSerializer):

    class Meta:
        model = ReportCategory
        exclude = ('is_active', 'is_internal', 'permission_groups')


class ReportQueryFieldSerializer(serializers.ModelSerializer):

    class Meta:
        model = ReportQueryField
        fields = ('name', 'is_required', 'is_array',)


class ReportSerializer(serializers.ModelSerializer):

    class Meta:
        model = Report
        exclude = ('is_active', 'is_internal')

    category = ReportCategorySerializer(read_only=True)
    has_filter = serializers.BooleanField(read_only=True)
    fields = ReportQueryFieldSerializer(many=True)

    def to_representation(self, instance):
        data = super().to_representation(instance)

        filter_form_html = cache.get(f'report.filter-form-{instance.pk}')
        if not filter_form_html:
            filter_form = instance.get_filter_form_class()
            filter_form_html = filter_form().as_table()
            cache.set(f'report.filter-form-{instance.pk}', filter_form_html, timeout=60 * 60 * 24)

        data['filter_form'] = filter_form_html
        data['fields'] = {data['name']: data for data in data['fields']}

        return data


class ReportManySerializer(serializers.ModelSerializer):

    class Meta:
        model = Report
        fields = ('id', 'name', 'category', 'description')

    category = ReportCategorySerializer(read_only=True)


class ReportResultSerializer(serializers.ModelSerializer):

    report = ReportManySerializer(read_only=True)
    status_display = serializers.CharField(source='get_status_display', read_only=True)
    files_display = serializers.SerializerMethodField()

    class Meta:
        model = ReportResult
        fields = '__all__'

    def get_files_display(self, instance):
        files = []
        for file in instance.files:
            files.append(
                (f'{settings.MEDIA_URL}{file}', file.split('.')[-1])
            )

        return files
