
from django.urls import include, path
from rest_framework import routers

from . import views


router = routers.DefaultRouter()
router.register('report', views.ReportView)
router.register('report-result', views.ReportResultView)

urlpatterns = [
    path('', include(router.urls)),
]
