
import os
import datetime
import traceback

from celery.exceptions import SoftTimeLimitExceeded
from django.conf import settings
from django.utils import timezone

from pytils.translit import slugify

from celery import shared_task

from . import generators
from .models import Report, ReportResult, ReportView
from .utils import import_reports_from_repository


@shared_task(bind=True)
def import_reports(self):
    import_reports_from_repository(False)


@shared_task(bind=True)
def cleanup_report_results(self):
    yesterday = timezone.now() - datetime.timedelta(days=1)
    ReportResult.objects.filter(created__lte=yesterday).delete()


@shared_task(bind=True)
def update_materialized_views(self, ids=None):
    qs = ReportView.objects.filter(is_materialized=True)
    if ids:
        qs = qs.filter(id__in=ids)

    for view in qs:
        view.refresh_view()


@shared_task(bind=True)
def generate_report_result(self, report_result_id):
    try:
        report_result = ReportResult.objects.get(id=report_result_id)
    except ReportResult.DoesNotExist:
        return

    try:
        report = report_result.report
        results = report.get_queries_result(report_result.filter_data, None)

        html = report.render_html_result(
            filter=report_result.filter_data,
            results=results
        )

        today = timezone.now().date().strftime('%Y-%m-%d')
        file_name = slugify(report.name + (report_result.filter_data['display'] or ''))[:100]
        base_dir = os.path.join(settings.MEDIA_ROOT, f'report/{today}')
        if not os.path.isdir(base_dir):
            os.makedirs(base_dir)

        if report.generate_spreadsheet:
            file_path = f'report/{today}/{file_name}-{report.id}.xlsx'
            file_dir = os.path.join(settings.MEDIA_ROOT, file_path)

            with open(file_dir, 'wb') as f:
                if report.template_html:
                    output = generators.generate_excel(html, f)
                else:
                    output = generators.generate_excel_from_results(
                        results,
                        title=report_result.filter_data['display'],
                        output=f
                    )

                output.close()
                report_result.files.append(file_path)

        if report.generate_pdf:
            file_path = f'report/{today}/{file_name}-{report.id}.pdf'
            file_dir = os.path.join(settings.MEDIA_ROOT, file_path)

            pdf_results = report.get_queries_result(report_result.filter_data, 1, 100)
            pdf_html = report.render_html_result(
                filter=report_result.filter_data,
                results=pdf_results
            )

            with open(file_dir, 'wb') as f:
                f.write(generators.generate_pdf(pdf_html, report.pdf_orientation))
                f.close()
                report_result.files.append(file_path)

        total = 0
        for name, result in results.items():
            total += result['count']

        if total < report.records_per_page:
            report_result.html = html
        else:
            results = report.get_queries_result(report_result.filter_data, 1)
            report_result.html = report.render_html_result(
                filter=report_result.filter_data,
                results=results
            )

        del html, results

        report_result.status = ReportResult.SUCCESS
        report_result.save()

    except SoftTimeLimitExceeded:
        report_result.html = 'Время, отведенное на выполнение отчета закончилось'
        report_result.status = ReportResult.FAILED
        report_result.save()

    except Exception as e:
        #raven_client.captureException(extra={'report_id': report_result.report.id})
        report_result.html = traceback.format_exc()
        report_result.status = ReportResult.FAILED
        report_result.save()
