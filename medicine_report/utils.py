
import requests
import zipfile
import tempfile
from io import BytesIO

from import_export.formats import base_formats

from .models import Report
from . import import_export


def import_reports_from_repository(raise_errors=False):
    r = requests.get('https://bitbucket.org/ilya_muhortov/medicine-fixtures/get/report.zip')
    if r.status_code != 200:
        return

    z = zipfile.ZipFile(BytesIO(r.content))
    temp_dir = tempfile.mkdtemp()
    z.extractall(path=temp_dir)

    fixtures_map = {}
    for file in z.namelist():
        if file.endswith('.json'):
            file_name = file.split('/')[-1].replace('.json', '')
            fixtures_map[file_name.lower()] = f'{temp_dir}/{file}'

    resources = [
        import_export.ReportCategoryResource,
        import_export.ReportCatalogResource,
        import_export.ReportCatalogValueResource,
        import_export.ReportResource,
        import_export.ReportQueryResource,
        import_export.ReportQueryFieldResource,
        import_export.ReportTemplateVariableResource,
        import_export.ReportFunctionResource,
        import_export.ReportViewResource
    ]

    resource_map = []
    for resource in resources:
        model_class = resource._meta.model
        app_label = model_class._meta.app_label.lower()
        resource_map.append([f'{app_label}.{model_class.__name__.lower()}', resource])

    for model_name, resource_class in resource_map:
        if model_name in fixtures_map:
            with open(fixtures_map[model_name]) as f:
                input_format = base_formats.JSON()
                data = input_format.create_dataset(in_stream=f.read())
                resource_class().import_data(data, dry_run=False, raise_errors=raise_errors)

    Report.clear_cache()
