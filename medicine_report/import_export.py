
from django.contrib.contenttypes.models import ContentType
from django.db.utils import ProgrammingError

from import_export import resources, fields

from .models import *

__all__ = ['ReportCategoryResource', 'ReportResource', 'ReportQueryResource', 'ReportCatalogResource',
           'ReportCatalogValueResource', 'ReportQueryFieldResource', 'ReportViewResource',
           'ReportTemplateVariableResource']


class JSONWidget(fields.widgets.Widget):

    def clean(self, value, row=None, *args, **kwargs):
        return value

    def render(self, value, obj=None):
        return value


class ReportCategoryResource(resources.ModelResource):

    class Meta:
        model = ReportCategory
        skip_unchanged = True
        exclude = ('is_active', 'permission_groups',)

    def get_queryset(self):
        return super().get_queryset().filter(is_internal=True)


class ReportResource(resources.ModelResource):

    class Meta:
        model = Report
        skip_unchanged = True
        exclude = ('is_active', 'updated')

    def get_queryset(self):
        return super().get_queryset().filter(is_internal=True)

    def after_import(self, dataset, *args, **kwargs):
        super().after_import(dataset, *args, **kwargs)
        imported_ids = [row['id'] for row in dataset.dict]
        Report.objects.filter(is_internal=True).exclude(id__in=imported_ids).delete()


class ReportQueryResource(resources.ModelResource):

    class Meta:
        model = ReportQuery
        skip_unchanged = True

    def get_queryset(self):
        return super().get_queryset().filter(report__is_internal=True)

    def after_import(self, dataset, *args, **kwargs):
        super().after_import(dataset, *args, **kwargs)
        imported_ids = [row['id'] for row in dataset.dict]
        ReportQuery.objects.filter(report__is_internal=True).exclude(id__in=imported_ids).delete()


class ReportCatalogResource(resources.ModelResource):

    content_type = resources.Field()
    content_type_filter = fields.Field(
        column_name='content_type_filter',
        attribute='content_type_filter',
        widget=JSONWidget()
    )

    class Meta:
        model = ReportCatalog
        skip_unchanged = False

    def get_queryset(self):
        return super().get_queryset().all()

    def dehydrate_content_type(self, instance):
        if instance.content_type_id:
            return f'{instance.content_type.app_label}.{instance.content_type.model}'

    def import_obj(self, obj, data, *args):
        content_type = data.pop('content_type')
        super().import_obj(obj, data, *args)
        if content_type:
            app_label, model = content_type.split('.')
            try:
                obj.content_type = ContentType.objects.get_by_natural_key(app_label, model)
            except ContentType.DoesNotExist:
                pass


class ReportCatalogValueResource(resources.ModelResource):

    class Meta:
        model = ReportCatalogValue
        skip_unchanged = True

    def get_queryset(self):
        return super().get_queryset().all()

    def after_import(self, dataset, *args, **kwargs):
        super().after_import(dataset, *args, **kwargs)
        imported_ids = [row['id'] for row in dataset.dict]
        ReportCatalogValue.objects.exclude(id__in=imported_ids).delete()


class ReportQueryFieldResource(resources.ModelResource):

    class Meta:
        model = ReportQueryField
        skip_unchanged = True

    def get_queryset(self):
        return super().get_queryset().filter(report__is_internal=True)

    def import_obj(self, obj, data, *args):
        super().import_obj(obj, data, *args)

        query_field = ReportQueryField.objects.filter(report=obj.report, name=obj.name).first()
        if query_field and query_field.id != obj.id:
            query_field.delete()

    def after_import(self, dataset, *args, **kwargs):
        super().after_import(dataset, *args, **kwargs)
        imported_ids = [row['id'] for row in dataset.dict]
        ReportQueryField.objects.filter(report__is_internal=True).exclude(id__in=imported_ids).delete()


class ReportFunctionResource(resources.ModelResource):

    class Meta:
        model = ReportFunction
        skip_unchanged = True
        exclude = ('updated',)

    def before_save_instance(self, instance, using_transactions, dry_run):
        original = ReportFunction.objects.filter(id=instance.id).first()
        try:
            ReportFunction.create_or_update_function(instance.sql, original.sql if original else None)
        except ProgrammingError:
            pass


class ReportViewResource(resources.ModelResource):

    class Meta:
        model = ReportView
        skip_unchanged = False
        exclude = ('updated',)

    def before_import(self, dataset, using_transactions, dry_run, **kwargs):
        for report_view in ReportView.objects.all():
            report_view.drop()

    def before_save_instance(self, instance, using_transactions, dry_run):
        ReportView.create_view(instance.sql, instance.name, instance.is_materialized)

    def after_import(self, dataset, *args, **kwargs):
        super().after_import(dataset, *args, **kwargs)
        imported_ids = [row['id'] for row in dataset.dict]
        for report_view in ReportView.objects.exclude(id__in=imported_ids):
            ReportView.drop_view(report_view.name, report_view.is_materialized)
            report_view.delete()


class ReportTemplateVariableResource(resources.ModelResource):

    class Meta:
        model = ReportTemplateVariable
        skip_unchanged = True
        exclude = ('value',)

    def after_import(self, dataset, *args, **kwargs):
        super().after_import(dataset, *args, **kwargs)
        imported_ids = [row['id'] for row in dataset.dict]
        ReportTemplateVariable.objects.filter(id__lte=1000).exclude(id__in=imported_ids).delete()
