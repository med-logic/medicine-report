
from django.core.management.base import BaseCommand, CommandError

from medicine.report.models import Report
from medicine.report import generators


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument(
            'ids',
            nargs='+',
            type=int
        )

    def handle(self, *args, **options):
        qs = Report.objects.all()
        success = 0
        failed = 0

        if options['ids'][0] != 0:
            qs = qs.filter(id__in=options['ids'])

        for report in qs:
            if report.queries.count() == 0:
                self.stdout.write(f'#{report.id} {report.name} - Отсутствуют запросы', style_func=self.style.WARNING)
                self.stdout.write('-' * 50)
                continue

            try:
                initial_data = {}
                for field in report.fields.filter(test_value__isnull=False):
                    initial_data[field.name] = field.test_value

                filter_data = report.get_filter_data(initial_data)

                results = report.get_queries_result(filter_data)
                html = report.render_html_result(
                    filter=filter_data,
                    results=results
                )

                generators.generate_excel(html)
                self.stdout.write(f'#{report.id} {report.name}', style_func=self.style.SUCCESS)

                for _, result in results.items():
                    if result['count'] == 0:
                        self.stdout.write(f'(¬_¬) %s' % result['title'], style_func=self.style.WARNING)
                self.stdout.write(f'\ (•◡•) /', style_func=self.style.SUCCESS)
                success += 1

            except (report.Error, Exception) as e:
                self.stderr.write(f'#{report.id} {report.name}')
                self.stderr.write(e.__str__())
                self.stderr.write('¯\_(ツ)_/¯')
                failed += 1

            self.stdout.write('-' * 50)

        self.stdout.write(f'{success} \ (•◡•) /', style_func=self.style.SUCCESS)
        self.stderr.write(f'{failed} ¯\_(ツ)_/¯')
