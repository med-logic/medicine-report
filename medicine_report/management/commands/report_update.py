
from django.core.management.base import BaseCommand, CommandError

from medicine.report.utils import import_reports_from_repository


class Command(BaseCommand):

    def handle(self, *args, **options):
        import_reports_from_repository(True)
