
from django.core.management.base import BaseCommand, CommandError
from django.core import management


class Command(BaseCommand):

    def handle(self, *args, **options):
        management.call_command('importdata', 'report.ReportCategory', 'report.ReportCatalog', 'report.ReportFunction',
                                'report.ReportView', 'report.ReportCatalogValue', 'report.Report', 'report.ReportQuery',
                                'report.ReportQueryField', 'report.ReportTemplateVariable')
