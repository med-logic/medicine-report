
from django.core.management.base import BaseCommand, CommandError
from django.core import management


class Command(BaseCommand):

    def handle(self, *args, **options):
        management.call_command('exportdata', '--model=report.ReportCategory')
        management.call_command('exportdata', '--model=report.ReportCatalog')
        management.call_command('exportdata', '--model=report.ReportCatalogValue')
        management.call_command('exportdata', '--model=report.ReportFunction')
        management.call_command('exportdata', '--model=report.ReportView')

        management.call_command('exportdata', '--model=report.Report')
        management.call_command('exportdata', '--model=report.ReportQuery')
        management.call_command('exportdata', '--model=report.ReportQueryField')
        management.call_command('exportdata', '--model=report.ReportTemplateVariable')
