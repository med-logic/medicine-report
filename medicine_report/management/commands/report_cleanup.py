
from django.core.management.base import BaseCommand, CommandError

from medicine.report.models import *


class Command(BaseCommand):

    def handle(self, *args, **options):
        ReportCatalog.objects.all().delete()
        ReportFunction.objects.all().delete()
        ReportResult.objects.all().delete()
        ReportCategory.objects.all().delete()
        Report.objects.all().delete()
        ReportView.objects.all().delete()
        ReportTemplateVariable.objects.all().delete()
