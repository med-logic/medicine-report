
import re

from django.db import models
from django.db import connections, InternalError, transaction
from django.conf import settings
from django.db.utils import ProgrammingError

__all__ = ['ReportFunction']


class ReportFunction(models.Model):

    name = models.CharField(max_length=255, unique=True, verbose_name='Название')
    description = models.TextField(null=True, blank=True, verbose_name='Описание')
    sql = models.TextField(verbose_name='SQL')
    updated = models.DateTimeField(auto_now=True, editable=False)

    class Meta:
        verbose_name = 'Функция'
        verbose_name_plural = 'Функции'
        default_permissions = ()

    def __str__(self):
        return self.name

    @staticmethod
    def get_function_name(sql):
        search = re.search('^CREATE OR REPLACE FUNCTION (?P<name>md_[\w]+)', sql)
        if search and search.group('name'):
            return search.group('name')
        return None

    @staticmethod
    def get_drop_sql(function_name):
        cursor = connections['default'].cursor()
        sql = """
              SELECT 
                format('DROP FUNCTION IF EXISTS %s', oid::regprocedure) AS stmt
              FROM
                pg_catalog.pg_proc
              WHERE
                proname = '{name}'
              ORDER BY 1
        """.format(name=function_name)

        cursor.execute(sql)
        sql_list = []
        for row in cursor.fetchall():
            sql_list.append(row[0])

        return '; '.join(sql_list)

    @classmethod
    def create_or_update_function(cls, sql, previous_sql):
        cursor = connections['default'].cursor()

        sid = transaction.savepoint(using='default')
        cls.drop_function(previous_sql or sql)

        try:
            cursor.execute(sql)
        except ProgrammingError as e:
            transaction.savepoint_rollback(sid, using='default')
            raise ProgrammingError(e)

    @classmethod
    def drop_function(cls, sql):
        cursor = connections['default'].cursor()
        function_name = cls.get_function_name(sql)

        drop_sql = cls.get_drop_sql(function_name)
        if drop_sql:
            cursor.execute(drop_sql)
