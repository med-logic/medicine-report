
from django.db import models
from django.core import validators

__all__ = ['ReportTemplateVariable']


class ReportTemplateVariable(models.Model):

    name = models.CharField(max_length=100, unique=True, verbose_name='Название', validators=[
        validators.RegexValidator(regex='^[A-Z]+[A-Z0-9_]*$')
    ])
    value = models.TextField(null=True, blank=True, verbose_name='Значение')

    class Meta:
        verbose_name = 'Переменная'
        verbose_name_plural = 'Переменные в шаблон'

    def __str__(self):
        return self.name

    @classmethod
    def as_dict(cls):
        return {variable.name: variable.value for variable in cls.objects.all()}
