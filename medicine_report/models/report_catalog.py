
from django.db import models
from django.contrib.postgres.fields import JSONField
from django.contrib.contenttypes.models import ContentType
from django.db.models import Q

__all__ = ['ReportCatalog', 'ReportCatalogValue']


class ReportCatalog(models.Model):

    name = models.CharField(max_length=255, verbose_name='Название')
    content_type = models.ForeignKey(ContentType, on_delete=models.SET_NULL, null=True, blank=True, verbose_name='Справочник')
    content_type_filter = JSONField(null=True, blank=True, verbose_name='Фильтр')

    class Meta:
        verbose_name = 'Справочник'
        verbose_name_plural = 'Справочники'

    def __str__(self):
        return self.name

    def get_choices(self, add_empty):
        choices = []
        if add_empty:
            choices.append(('', '-'))

        for item in self.values.all():
            choices.append((item.value, item.name))

        if self.content_type:
            model_class = self.content_type.model_class()

            get_display = lambda item: item.__str__()
            if hasattr(model_class, 'display'):
                get_display = lambda item: item.display

            qs = self.get_choices_queryset()

            for item in qs[:300]:
                choices.append((item.pk, get_display(item)))

        return choices

    def get_choices_queryset(self):
        model_class = self.content_type.model_class()

        if hasattr(model_class, 'list'):
            qs = model_class.list()
        elif hasattr(model_class.objects, 'list'):
            qs = model_class.objects.list()
        else:
            qs = model_class.objects.all()

        if self.content_type_filter and isinstance(self.content_type_filter, (dict,)):
            conditions = []
            for key, value in self.content_type_filter.items():
                conditions.append(Q(**{key: value}))

            if len(conditions) > 0:
                query = conditions.pop(0)
                for condition in conditions:
                    query &= condition

                qs = qs.filter(query)

        return qs

    @staticmethod
    def get_content_type_queryset():
        qs = ContentType.objects.filter(
            Q(app_label='directory') |
            Q(app_label='lpu', model='lpu') |
            Q(app_label='lpu', model='department') |
            Q(app_label='lpu', model='district') |
            Q(app_label='catalog') |
            Q(app_label='account', model='user')
        )
        return qs


class ReportCatalogValue(models.Model):

    report_catalog = models.ForeignKey(ReportCatalog, on_delete=models.CASCADE, related_name='values')
    value = models.CharField(max_length=255, verbose_name='Значение')
    name = models.CharField(max_length=255, verbose_name='Название')

    class Meta:
        verbose_name = 'Значение'
        verbose_name_plural = 'Значения'
        default_permissions = ()

    def __str__(self):
        return self.name
