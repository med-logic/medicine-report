
import re
from collections import OrderedDict

from django import forms
from django.db import models
from django.conf import settings
from django.template.loader import render_to_string
from django.db import connections
from django.core import validators
from django.db.utils import ProgrammingError
from django.core.cache import cache


from medicine_report.forms import ReportFilterForm

from .report_template_variable import ReportTemplateVariable

__all__ = ['Report', 'ReportCategory', 'ReportQuery']


class ReportException(Exception):
    pass


class ReportRenderException(Exception):
    pass


class ReportManager(models.Manager):

    def for_user(self, user):
        qs = self.filter(is_active=True, category__is_active=True)

        if user.is_superuser is False:
            groups = [group for group in user.groups.all()]
            qs = qs.filter(category__permission_groups__in=groups).distinct('id')

        return qs


class ReportCategory(models.Model):

    name = models.CharField(max_length=200, unique=True, verbose_name='Название')
    is_active = models.BooleanField(default=True, verbose_name='Активность')
    is_internal = models.BooleanField(default=True, verbose_name='Системный')
    permission_groups = models.ManyToManyField('auth.Group', blank=True, verbose_name='Группы')

    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'

    def __str__(self):
        return self.name


class Report(models.Model):

    Error = ReportException
    QueryError = ReportException
    RenderError = ReportRenderException
    ValidationError = forms.ValidationError

    TEMPLATE_DEFAULT = 'default.table.html'
    TEMPLATES = [
        (None, 'Шаблон'),
        ('default.table.html', 'Обычная таблица'),
    ]

    name = models.CharField(max_length=55, verbose_name='Название')
    category = models.ForeignKey(ReportCategory, null=True, blank=True, on_delete=models.SET_NULL,
                                 verbose_name='Категория')
    is_active = models.BooleanField(default=True, verbose_name='Активность')
    is_internal = models.BooleanField(default=True, verbose_name='Системный')
    is_async = models.BooleanField(default=False, verbose_name='Асинхронный')

    description = models.TextField(null=True, blank=True, verbose_name='Описание')
    result_per_page = models.PositiveIntegerField(default=100, validators=[
        validators.MinValueValidator(0),
        validators.MaxValueValidator(500)
    ], verbose_name='Записей на страницу')
    template_name = models.CharField(max_length=32, null=True, blank=True, choices=TEMPLATES, verbose_name='Шаблон')
    template_html = models.TextField(null=True, blank=True, verbose_name='HTML шаблон')
    generate_spreadsheet = models.BooleanField(default=True, verbose_name='Генерация таблицы')
    generate_pdf = models.BooleanField(default=True, verbose_name='Генерация PDF')
    orientation = models.CharField(max_length=16, null=True, verbose_name='Ориентация')
    updated = models.DateTimeField(auto_now=True, editable=False)

    class Meta:
        verbose_name = 'Отчет'
        verbose_name_plural = 'Отчеты'

    objects = ReportManager()

    def __str__(self):
        return self.name

    @property
    def display(self):
        return self.__str__()

    def has_filter(self):
        return self.fields.count() > 0

    def get_filter_form_class(self):
        if not hasattr(self, '_filter_form_class'):
            fields = {}
            for field in self.fields.all():
                fields[field.name] = field.get_field_class()

            self._filter_form_class = type('AutoReportFilterForm', (ReportFilterForm,), fields)

        return self._filter_form_class

    @property
    def filter_form_class(self):
        return self.get_filter_form_class()

    @property
    def cursor(self):
        return connections[settings.DATABASE_READONLY_CONNECTION].cursor()

    @property
    def records_per_page(self):
        return self.result_per_page

    def get_filter_data(self, data):
        filter_data = {
            'display': None
        }

        if self.has_filter():
            form_class = self.filter_form_class
            if data:
                form = form_class(data)
                if form.is_valid():
                    for key, val in form.cleaned_data.items():
                        filter_data[key] = form[key].field.prepare_value(val)

                    filter_data['display'] = form.get_filter_display()

                else:
                    raise self.ValidationError(form.get_errors_display())

            else:
                raise self.ValidationError({})

        return filter_data

    def get_queries_result(self, filter_data, page=None, results_per_page=None):
        cursor = self.cursor

        if results_per_page is None:
            results_per_page = self.result_per_page

        result = OrderedDict()
        for query in self.queries.all():
            try:
                cursor.execute(query.get_clean_sql(), filter_data)
            except (ProgrammingError, Exception) as e:
                raise self.Error(e.__str__())

            count = cursor.rowcount
            columns = [column.name for column in cursor.description]

            if count <= results_per_page or page is None:
                lines = [dict(zip(columns, row)) for row in cursor.fetchall()]
            else:
                if page == 1:
                    lines = [dict(zip(columns, row)) for row in cursor.fetchall()[:results_per_page]]
                else:
                    try:
                        cursor.execute(query.get_sql_limit_offset(page, results_per_page), filter_data)
                    except (ProgrammingError, Exception) as e:
                        raise self.Error(e.__str__())

                    lines = [dict(zip(columns, row)) for row in cursor.fetchall()]

            result[query.name] = {
                'title': query.title,
                'columns': columns,
                'lines': lines,
                'count': count,
                'query': cursor.query.decode('utf-8')
            }

        return result

    def get_html_template(self):
        if self.template_html:
            return self.template_html
        elif self.template_name:
            return render_to_string(f'report/{self.template_name}')
        else:
            return render_to_string(f'report/{self.TEMPLATE_DEFAULT}')

    def render_html_result(self, **kwargs):
        kwargs.update(ReportTemplateVariable.as_dict())

        try:
            return jinja2.jinja_env.from_string(self.get_html_template()).render(**kwargs)
        except jinja2.TemplateSyntaxError as e:
            raise self.RenderError(f'Line {e.lineno} - {e.__str__()}')
        except jinja2.TemplateError as e:
            raise self.RenderError(f'{e.__class__.__name__}: {e}')
        except Exception as e:
            raise self.RenderError(e.__str__())

    @staticmethod
    def clear_cache():
        if hasattr(cache, 'delete_pattern'):
            cache.delete_pattern('report.*')


class ReportQuery(models.Model):

    report = models.ForeignKey(Report, on_delete=models.CASCADE, related_name='queries')
    name = models.CharField(max_length=10, default='result', validators=[validators.RegexValidator('\w+')],
                            verbose_name='Название переменной')
    title = models.CharField(max_length=30, verbose_name='Заголовок')
    sql = models.TextField(verbose_name='Sql запрос')

    def clean(self):
        if self.sql:
            self.sql = self.sql.rstrip(';')

    def get_clean_sql(self):
        sql_text = re.sub('--(.*|[\r\n])', '', self.sql)
        sql_text = sql_text.strip(';')
        return sql_text

    def get_sql_limit_offset(self, page, per_page):
        return '%s LIMIT %d OFFSET %d' % (self.get_clean_sql(), per_page, (page - 1) * per_page)

    class Meta:
        verbose_name = 'SQL запрос'
        verbose_name_plural = 'SQL запросы'
        ordering = ('id',)
        default_permissions = ()

    def __str__(self):
        return f'{self.name}, #{self.id}'
