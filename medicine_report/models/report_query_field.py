
import datetime
from django.db import models
from django.utils.module_loading import import_string
from django import forms

from medicine_report import fields as report_fields

__all__ = ['ReportQueryField']


class ReportQueryField(models.Model):

    FIELD_CHAR = 'Char'
    FIELD_TEXT = 'Text'
    FIELD_INT = 'Int'
    FIELD_BOOL = 'Bool'
    FIELD_NULLBOOL = 'NullBool'
    FIELD_DATE = 'Date'
    FIELD_DATE_MONTH_YEAR = 'DateMonthYear'
    FIELD_CHOICE = 'Choice'

    FIELD_PYTHON_TYPE = {
        FIELD_DATE: datetime.datetime,
        FIELD_DATE_MONTH_YEAR: datetime.datetime,
        FIELD_CHAR: str,
        FIELD_TEXT: str,
        FIELD_INT: int,
        FIELD_BOOL: bool,
        FIELD_CHOICE: str
    }

    FIELD_CLASS = {
        FIELD_DATE: 'medicine_report.fields.Date',
        FIELD_INT: 'medicine_report.fields.Int',
        FIELD_CHAR: 'medicine_report.fields.Char',
        FIELD_TEXT: 'medicine_report.fields.Text',
        FIELD_BOOL: 'medicine_report.fields.Bool',
        FIELD_CHOICE: 'medicine_report.fields.Choice',
        FIELD_NULLBOOL: 'medicine_report.fields.NullBool',
        FIELD_DATE_MONTH_YEAR: 'medicine_report.fields.DateMonthYear',
    }

    FIELDS = (
        (FIELD_DATE, 'Дата'),
        (FIELD_DATE_MONTH_YEAR, 'Дата (месяц/год)'),
        (FIELD_INT, 'Число'),
        (FIELD_CHAR, 'Строка'),
        (FIELD_TEXT, 'Текст'),
        (FIELD_BOOL, 'Булево'),
        (FIELD_CHOICE, 'Выбор'),
    )

    report = models.ForeignKey('medicine_report.Report', on_delete=models.CASCADE, related_name='fields')
    field_type = models.CharField(choices=FIELDS, max_length=20, default=FIELD_CHAR, verbose_name='Тип поля')
    name = models.CharField(max_length=100, verbose_name='Имя')
    label = models.CharField(max_length=100, null=True, verbose_name='Название')
    is_required = models.BooleanField(default=False, verbose_name='Обязательно')
    is_array = models.BooleanField(default=False, verbose_name='Массив')
    catalog = models.ForeignKey('medicine_report.ReportCatalog', on_delete=models.SET_NULL, null=True, blank=True, verbose_name='Значения')
    default_value = models.CharField(max_length=200, null=True, blank=True, verbose_name='Значение по умолчанию')
    test_value = models.CharField(max_length=200, null=True, blank=True, verbose_name='Тестовое значение')
    sort = models.PositiveIntegerField(default=0, verbose_name='Порядок')

    class Meta:
        verbose_name = 'Поле'
        verbose_name_plural = 'Поля'
        default_permissions = ()
        ordering = ('sort',)
        unique_together = (
            ('report', 'name'),
        )

    def get_field_class(self):
        field_class = import_string(self.FIELD_CLASS[self.field_type])
        field_params = dict(
            label=self.label,
            required=self.is_required,
            initial=self.default_value
        )

        if self.field_type == self.FIELD_BOOL:
            if self.is_required is False:
                field_class = report_fields.NullBool
            else:
                field_params['required'] = False

        if self.catalog and self.field_type in [self.FIELD_CHAR, self.FIELD_INT, self.FIELD_CHOICE]:
            catalog = self.catalog
            if catalog.content_type:
                model_class = catalog.content_type.model_class()
                qs = catalog.get_choices_queryset()

                field_class = report_fields.ModelChoice
                field_params['queryset'] = qs

                if hasattr(model_class, 'get_autocomplete_url') and qs.count() > 300:
                    field_params['autocomplete_url'] = model_class.get_autocomplete_url()
                    field_params['primary_key'] = model_class._meta.pk.name

                if self.is_array:
                    field_class = report_fields.ModelMultipleChoice

                if self.is_required and qs.count() == 1:
                    field_params['widget'] = forms.HiddenInput
                    if not self.default_value:
                        if self.is_array:
                            field_params['initial'] = [qs.first()]
                        else:
                            field_params['initial'] = qs.first()

            else:
                field_class = report_fields.TypedChoice
                field_params['coerce'] = self.FIELD_PYTHON_TYPE[self.field_type]
                field_params['choices'] = catalog.get_choices(not self.is_required)

                if self.is_array:
                    field_class = report_fields.TypedMultipleChoiceField

        elif self.is_array:
            field_params['base_field'] = field_class()
            field_params['widget'] = field_class.widget
            field_class = report_fields.TypedArrayField

        if self.default_value:
            field_params['initial'] = field_class.get_initial_value(self.default_value)

        return field_class(**field_params)
