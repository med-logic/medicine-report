
from django.db import models
from django.contrib.postgres.fields import ArrayField, JSONField

from django.contrib.auth import get_user_model

USER_MODEL = get_user_model()

__all__ = ['ReportResult']


class ReportResult(models.Model):

    PROCESSING = 'processing'
    SUCCESS = 'success'
    FAILED = 'failed'

    STATUS_CHOICES = (
        (PROCESSING, 'В работе'),
        (SUCCESS, 'Завершена'),
        (FAILED, 'Ошибка')
    )

    user = models.ForeignKey(USER_MODEL, on_delete=models.CASCADE, verbose_name='Пользователь')
    report = models.ForeignKey('medicine_report.Report', on_delete=models.CASCADE)
    status = models.CharField(max_length=10, default=PROCESSING, choices=STATUS_CHOICES, verbose_name='Статус')
    filter_data = JSONField(default={})
    html = models.TextField(null=True, blank=True)
    files = ArrayField(models.CharField(max_length=300), default=[])

    task_id = models.CharField(max_length=100, null=True, blank=True, editable=False)
    created = models.DateTimeField(auto_now_add=True, editable=False, verbose_name='Дата создания')

    class Meta:
        default_permissions = ()
        verbose_name = 'Результат отчета'
        verbose_name_plural = 'Результаты отчетов'
        ordering = ('-created',)
