
from django.db import models
from django.db import connections, InternalError, transaction
from django.core import validators

__all__ = ['ReportView']


class ReportView(models.Model):

    name = models.CharField(max_length=255, unique=True, validators=[
        validators.RegexValidator('^[A-Za-zА-Яа-я0-9\._]+$')
    ], verbose_name='Название представления')
    is_materialized = models.BooleanField(default=False, verbose_name='Материальное представление')
    description = models.TextField(null=True, blank=True, verbose_name='Описание')
    position = models.PositiveIntegerField(default=0, verbose_name='Порядок')
    sql = models.TextField(verbose_name='SQL')
    updated = models.DateTimeField(auto_now=True, editable=False)

    class Meta:
        verbose_name = 'Представление'
        verbose_name_plural = 'Представления'
        default_permissions = ()
        ordering = ('position',)

    def __str__(self):
        return self.name

    def refresh_view(self):
        cursor = connections['default'].cursor()
        cursor.execute(f'REFRESH MATERIALIZED VIEW {self.name}')

    def drop(self):
        return self.__class__.drop_view(self.name, self.is_materialized)

    @staticmethod
    def get_view_create_sql(sql, name, is_materialized):
        return 'CREATE{materialized} VIEW "{name}" AS {sql}'.format(
            materialized=' MATERIALIZED' if is_materialized else '',
            name=name,
            sql=sql
        )

    @classmethod
    def drop_view(cls, name, is_materialized=False):
        cursor = connections['default'].cursor()
        sql = 'DROP{materialized} VIEW IF EXISTS "{name}" CASCADE'.format(
            materialized=' MATERIALIZED' if is_materialized else '',
            name=name
        )
        cursor.execute(sql)

    @classmethod
    def create_or_update_view(cls, sql, name, is_materialized):
        cursor = connections['default'].cursor()
        sid = transaction.savepoint(using='default')

        try:
            sql = cls.get_view_create_sql(sql, name, is_materialized)
            cls.drop_view(name, is_materialized)
            cursor.execute(sql)
        except Exception as e:
            transaction.savepoint_rollback(sid, using='default')
            raise ValueError(e)

    @classmethod
    def create_view(cls, sql, name, is_materialized):
        cursor = connections['default'].cursor()
        sid = transaction.savepoint(using='default')

        try:
            sql = cls.get_view_create_sql(sql, name, is_materialized)
            cursor.execute(sql)
        except Exception as e:
            transaction.savepoint_rollback(sid, using='default')
            raise ValueError(e)
