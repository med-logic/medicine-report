
import datetime
import re

from django import forms
from django.utils import six
from django.contrib.postgres.forms import JSONField
from django.contrib.postgres.forms import SimpleArrayField
from django.utils.encoding import force_text
from django.conf import settings
from django.utils.dates import MONTHS
from django.utils.dateformat import format

from rest_framework import serializers


class TextWidget(forms.TextInput):

    def render(self, name, value, attrs=None):
        return super().render(name, value, attrs={'class': 'form-control'})


class TextareaWidget(forms.Textarea):

    def render(self, name, value, attrs=None):
        return super().render(name, value, attrs={'class': 'form-control'})


class DateWidget(forms.TextInput):

    def render(self, name, value, attrs=None):
        return super().render(name, value, attrs={'widget-report-date': ''})


class SelectWidget(forms.Select):

    def render(self, name, value, attrs=None, choices=()):
        return super().render(name, value, attrs={'widget-report-select': ''},
                              choices=choices)


class SelectAutocompleteWidget(forms.Select):

    def render(self, name, value, attrs=None, choices=()):
        return super().render(name, value, attrs={'widget-report-autocomplete': ''},
                              choices=choices)


class SelectMultipleWidget(forms.SelectMultiple):

    def render(self, name, value, attrs=None, choices=()):
        return super().render(name, value, attrs={'widget-report-select': ''},
                              choices=choices)


class DateMonthYearWidget(forms.MultiWidget):

    def __init__(self, attrs=None):
        widgets = (
            forms.Select(attrs={'class': 'form-control'}, choices=list(MONTHS.items())),
            forms.NumberInput(attrs={'size': 8, 'class': 'form-control w-80'}),
        )
        super().__init__(widgets, attrs)

    def decompress(self, value):
        if value:
            value = datetime.datetime.strptime(value, settings.DATE_DISPLAY_FORMAT)
            return [value.month, value.year]
        return [None, None]

    def value_from_datadict(self, data, files, name):
        if name in data:
            return data[name]
        return super().value_from_datadict(data, files, name)

    def format_output(self, rendered_widgets):
        widgets = ' / '.join(rendered_widgets)
        return f'<div class="form-inline">{widgets}</div>'


class BaseFieldMixin(object):

    def to_representation(self, value):
        return value

    def get_value_display(self, value):
        return force_text(value)

    @staticmethod
    def get_initial_value(value):
        return value


class Int(BaseFieldMixin, forms.IntegerField):

    widget = TextWidget

    def prepare_value(self, value):
        return value


class Char(BaseFieldMixin, forms.CharField):

    widget = TextWidget

    def prepare_value(self, value):
        if value is None:
            return ''
        return super(Char, self).prepare_value(value)


class Text(Char):

    widget = TextareaWidget


class Date(BaseFieldMixin, forms.DateField):

    widget = DateWidget

    def to_representation(self, value):
        return serializers.DateField().to_representation(value)

    @staticmethod
    def get_initial_value(value):
        today = datetime.datetime.today()
        if value == 'today':
            return today.strftime(settings.DATE_DISPLAY_FORMAT)
        elif re.match('^(-|\+)(\d+) days$', value):
            match = re.search('^(-|\+)(\d+) days$', value)
            if match.group(1) == '+':
                date_value = today + datetime.timedelta(days=int(match.group(2)))
            else:
                date_value = today - datetime.timedelta(days=int(match.group(2)))
            return date_value.strftime(settings.DATE_DISPLAY_FORMAT)

        return value

    def get_value_display(self, value):
        if value:
            return value.strftime(settings.DATE_DISPLAY_FORMAT)


class DateMonthYear(Date):

    widget = DateMonthYearWidget
    input_formats = ['%m.%Y', '%Y-%m-%d']

    def to_python(self, value):
        if isinstance(value, (list, tuple)):
            value = '.'.join(list(filter(lambda x: x is not None, value)))

        return super().to_python(value)

    def get_value_display(self, value):
        if value:
            return format(value, 'F Y')


class Bool(BaseFieldMixin, forms.BooleanField):

    def get_value_display(self, value):
        return 'Да' if value else 'Нет'

    @staticmethod
    def get_initial_value(value):
        return False if value in ['0', 'False', 'false', 0, False] else True


class Choice(BaseFieldMixin, forms.ChoiceField):

    widget = SelectWidget

    def get_value_display(self, value):
        return force_text(value)


class TypedChoice(BaseFieldMixin, forms.TypedChoiceField):

    widget = SelectWidget

    def get_value_display(self, value):
        value = force_text(value)
        filtered = list(filter(lambda x: x[0] == value, self.choices))
        if len(filtered) > 0:
            return filtered[0][1]

        return value

    def prepare_value(self, value):
        if value in ['', None]:
            if self.coerce == str:
                return ''
            else:
                return None

        return value


class ModelChoice(BaseFieldMixin, forms.ModelChoiceField):

    widget = SelectWidget

    def __init__(self, *args, **kwargs):
        self.autocomplete_url = kwargs.pop('autocomplete_url', None)
        self.primary_key = kwargs.pop('primary_key', None)

        if self.autocomplete_url:
            self.widget = SelectAutocompleteWidget
            self._choices = ()

        super().__init__(*args, **kwargs)

    def widget_attrs(self, widget):
        attrs = super().widget_attrs(widget)
        if self.autocomplete_url:
            attrs['autocomplete-url'] = self.autocomplete_url
            attrs['primary-key'] = self.primary_key

        return attrs

    def label_from_instance(self, obj):
        if hasattr(obj, 'display'):
            return obj.display

        return super().label_from_instance(obj)


class ModelMultipleChoice(BaseFieldMixin, forms.ModelMultipleChoiceField):

    widget = SelectMultipleWidget

    def __init__(self, *args, **kwargs):
        self.autocomplete_url = kwargs.pop('autocomplete_url', None)
        self.primary_key = kwargs.pop('primary_key', None)

        if self.autocomplete_url:
            self.widget = SelectAutocompleteWidget
            self._choices = ()

        super().__init__(*args, **kwargs)

    def widget_attrs(self, widget):
        attrs = super().widget_attrs(widget)
        if self.autocomplete_url:
            attrs['autocomplete-url'] = self.autocomplete_url
            attrs['primary-key'] = self.primary_key

        return attrs

    def label_from_instance(self, obj):
        if hasattr(obj, 'display'):
            return obj.display

        return super().label_from_instance(obj)


class TypedMultipleChoiceField(BaseFieldMixin, forms.TypedMultipleChoiceField):

    widget = SelectMultipleWidget

    def prepare_value(self, value):
        return value


class NullBool(BaseFieldMixin, forms.NullBooleanField):

    def get_value_display(self, value):
        if value is None:
            return 'Неизвестно'

        return 'Да' if value else 'Нет'

    @staticmethod
    def get_initial_value(value):
        if value in ['0', 'False', 'false', 0, False]:
            return False
        elif value in ['1', 'True', 'true', 1, True]:
            return True


class TypedArrayField(BaseFieldMixin, SimpleArrayField):

    widget = TextWidget

    def prepare_value(self, value):
        return value

    def to_python(self, value):
        if isinstance(value, list) and len(value) > 0:
            value = value[0]

        return super().to_python(value)
