
from math import ceil
from collections import OrderedDict

from django.http import HttpResponse, HttpResponseBadRequest, Http404
from django.conf import settings

from rest_framework import mixins, viewsets
from rest_framework import permissions
from rest_framework import filters
from rest_framework.response import Response
from rest_framework.decorators import renderer_classes, detail_route, api_view
from pytils.translit import slugify


from . import tasks
from . import generators

from .models import Report, ReportResult
from .serializers import ReportSerializer, ReportManySerializer, ReportResultSerializer


class ReportResultView(mixins.RetrieveModelMixin,
                       mixins.ListModelMixin,
                       mixins.DestroyModelMixin,
                       viewsets.GenericViewSet):
    queryset = ReportResult.objects.all()
    serializer_class = ReportResultSerializer
    filter_backends = (filters.DjangoFilterBackend, filters.OrderingFilter)
    filter_fields = ('report',)

    def get_queryset(self):
        return super().get_queryset().filter(user=self.request.user)

    @detail_route(methods=['get', 'post'])
    def revoke(self, request, *args, **kwargs):
        instance = self.get_object()
        if instance.status == ReportResult.PROCESSING:
            #app.control.revoke([instance.task_id], terminate=True)
            instance.status = ReportResult.FAILED
            instance.save()

        return Response(self.serializer_class(instance=instance).data)


class ReportView(viewsets.ReadOnlyModelViewSet):
    queryset = Report.objects.none()
    permission_classes = (permissions.IsAuthenticated,)
    filter_backends = (filters.DjangoFilterBackend, filters.OrderingFilter)
    serializer_class = ReportSerializer
    serializer_many_class = ReportManySerializer

    def get_queryset(self):
        return Report.objects.for_user(self.request.user).select_related('category')

    def get_serializer(self, *args, **kwargs):
        many = kwargs.get('many', False)
        if many:
            serializer_class = self.serializer_many_class
        else:
            serializer_class = self.serializer_class

        kwargs['context'] = self.get_serializer_context()
        return serializer_class(*args, **kwargs)

    @detail_route(methods=['get', 'post'])
    def execute(self, request, *args, **kwargs):
        instance = self.get_object()

        try:
            current_page = int(request.data.get('page', 1))
        except (KeyError, ValueError, TypeError):
            current_page = 1

        queries = instance.queries.all()
        queries_count = queries.count()
        if queries_count == 0:
            return Response({'error': 'Запросы отсутсвуют'})

        try:
            filter_data = instance.get_filter_data(request.data)
            filter_data.update({
                'user_id': request.user.id
            })

            if instance.is_async:
                import json
                from django.core.serializers.json import DjangoJSONEncoder

                report_result_data = dict(
                    user=request.user,
                    report=instance,
                    filter_data=json.loads(json.dumps(filter_data, cls=DjangoJSONEncoder))
                )
                qs = ReportResult.objects.filter(**report_result_data).filter(status=ReportResult.PROCESSING)
                if not qs.exists():
                    report_result = ReportResult.objects.create(**report_result_data)
                    task_id = tasks.generate_report_result.apply_async(
                        args=[report_result.id],
                        delay=1000
                    )
                    report_result.task_id = task_id
                    report_result.save()

                    return Response(ReportResultSerializer(report_result).data)
                else:
                    return Response()

            results = instance.get_queries_result(filter_data, current_page)
            first_query_name = queries[0].name

            total = 0
            response_result = OrderedDict()
            for name, result in results.items():
                total += result['count']
                response_result[name] = dict(
                    count=result['count'],
                    query=result['query'],
                    title=result['title']
                )

            response = {
                'results': response_result,
                'total': total
            }

            if total > 0:
                response['html'] = instance.render_html_result(
                    filter=filter_data,
                    results=results
                )

                response.update({
                    'page': current_page,
                    'total_pages': 1,
                    'count': results[first_query_name]['count']
                })

                if instance.records_per_page > 0:
                    response['total_pages'] = int(ceil(results[first_query_name]['count'] / float(instance.records_per_page)))

        except instance.ValidationError as e:
            return Response({'error': e.__str__()})

        except (instance.Error, instance.RenderError) as e:
            raven_client.captureException(extra={'report_id': instance.id})
            return Response({'error': e.__str__()})

        return Response(response)

    @detail_route(methods=['GET'])
    def xls(self, request, *args, **kwargs):
        instance = self.get_object()

        if instance.generate_spreadsheet is False:
            return HttpResponseBadRequest()

        try:
            filter_data = instance.get_filter_data(request.GET)
            filter_data.update({
                'user_id': request.user.id
            })

            results = instance.get_queries_result(filter_data, None)

            if instance.template_html:
                html = instance.render_html_result(
                    filter=filter_data,
                    results=results
                )
                output = generators.generate_excel(html)
            else:
                output = generators.generate_excel_from_results(
                    results,
                    title=filter_data['display']
                )

            response = HttpResponse(
                output.read(),
                content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
            )
            response['Content-Disposition'] = 'attachment; filename=%s.xlsx' % slugify(instance.name)
            return response

        except instance.ValidationError as e:
            return Response({'error': e.__str__()})

        except (instance.Error, instance.RenderError) as e:
            raven_client.captureException(extra={'report_id': instance.id})
            return Response({'error': e.__str__()})

    @detail_route(methods=['GET'])
    def pdf(self, request, *args, **kwargs):
        instance = self.get_object()

        if instance.generate_pdf is False:
            return HttpResponseBadRequest()

        try:
            filter_data = instance.get_filter_data(request.GET)
            filter_data.update({
                'user_id': request.user.id
            })

            queries = instance.queries.all()
            results = instance.get_queries_result(filter_data, 1)

            html = instance.render_html_result(
                filter=filter_data,
                results=results,
            )

            if results[queries[0].name]['count'] > 100:
                return HttpResponseBadRequest('Слишком много для pdf')

            response = HttpResponse(
                pdf.generate_pdf(html, orientation=instance.pdf_orientation),
                content_type='application/pdf'
            )
            response['Content-Disposition'] = f'filename="{slugify(instance.name)}.pdf"'
            return response

        except instance.ValidationError as e:
            return Response({'error': e.__str__()})

        except (instance.Error, instance.RenderError) as e:
            raven_client.captureException(extra={'report_id': instance.id})
            return Response({'error': e.__str__()})
