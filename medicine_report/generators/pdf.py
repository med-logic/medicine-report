
#from medicine.core import pdf


def generate_pdf(html, orientation):
    return pdf.generate_pdf(html, orientation=orientation)
