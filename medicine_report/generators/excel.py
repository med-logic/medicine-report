
import xlsxwriter
import io
import re
import string
import copy
import decimal
import datetime

from lxml import etree


def get_format_attributes(element, attributes=None):
    if attributes is None:
        attributes = {}
    else:
        attributes = copy.copy(attributes)

    for attr in ['rotation', 'align', 'valign', 'border', 'text_wrap', 'font_size', 'num_format', 'bottom', 'data_type',
                 'top', 'left', 'right', 'border_color', 'bottom_color', 'top_color', 'left_color', 'right_color',
                 'shrink', 'indent', 'center_across', 'text_justlast', 'hidden', 'italic', 'bold', 'font_color']:
        if element.get(attr, None):
            if element.attrib[attr].isdigit():
                attributes[attr] = int(element.attrib[attr])
            else:
                attributes[attr] = element.attrib[attr]

    return attributes


def convert_int(value):
    try:
        return int(value)
    except:
        return value


def convert_decimal(value):
    try:
        return decimal.Decimal(value)
    except:
        return value


def convert_date(value):
    formats = ['%Y-%m-%d', '%d.%m.%Y']
    for date_format in formats:
        try:
            return datetime.datetime.strptime(value, date_format).date()
        except ValueError:
            pass

    return value


def generate_excel(html, output=None):
    if output is None:
        output = io.BytesIO()
        workbook = xlsxwriter.Workbook(output, {'in_memory': True})
    else:
        workbook = xlsxwriter.Workbook(output)

    text_format = workbook.add_format()
    text_format.set_text_wrap()

    tables = etree.fromstring(f'<div>{html}</div>')

    worksheet_names = []
    for index, table in enumerate(tables.xpath('//table')):
        worksheet_name = f'Страница {index + 1}'
        caption = table.find('caption')
        if caption is not None and caption.text:
            worksheet_name = caption.text[:30]

        if worksheet_name in worksheet_names:
            worksheet_name = f'Страница {index + 1}'

        worksheet_names.append(worksheet_name)

        worksheet = workbook.add_worksheet(name=worksheet_name)
        worksheet.set_column(0, 100, 30)

        alphabet = \
            [letter for letter in string.ascii_uppercase] + \
            [f'A{letter}' for letter in string.ascii_uppercase]

        table_format_attributes = get_format_attributes(table)
        if table.get('freeze'):
            worksheet.freeze_panes(int(table.get('freeze')), 0)

        busy_cells = []
        row_index = 1
        for group in table.xpath('thead|tbody|tfoot'):
            group_format_attributes = get_format_attributes(group, table_format_attributes)

            for row in group.xpath('tr'):
                if row.get('height'):
                    worksheet.set_row(row_index - 1, float(row.get('height')))

                col_index = 0
                tr_format_attributes = get_format_attributes(row, group_format_attributes)

                for cell in row.xpath('th|td'):
                    cell_format_attributes = get_format_attributes(cell, tr_format_attributes)
                    data_type = cell_format_attributes.pop('data_type', None)

                    while f'{alphabet[col_index]}{row_index}' in busy_cells:
                        col_index += 1

                    text = []
                    for nested_element in cell.iter():
                        if nested_element.text is not None:
                            text.append(nested_element.text)

                    text = ' '.join(filter(lambda item: item is not None and len(item.strip()) > 0, text))
                    if text:
                        text = text.strip()
                        if 'num_format' in cell_format_attributes:
                            text = decimal.Decimal(text)

                    if data_type == 'int':
                        text = int(text)
                    elif data_type == 'date':
                        text = convert_date(text)
                        if 'num_format' not in cell_format_attributes:
                            cell_format_attributes['num_format'] = 'dd.mm.yyyy'
                    elif data_type == 'decimal':
                        text = convert_decimal(text)
                        if 'num_format' not in cell_format_attributes:
                            cell_format_attributes['num_format'] = '0.00'

                    cell_format = workbook.add_format(properties=cell_format_attributes)

                    if cell.get('merge'):
                        pattern = '^(?P<start_col>[A-Z]+)(?P<start_row>\d*):(?P<end_col>[A-Z]+)(?P<end_row>\d*)$'
                        if re.match(pattern, cell.get('merge')):
                            search = re.search(pattern, cell.get('merge'))
                            start_col = search.group('start_col')
                            start_row = search.group('start_row') if search.group('start_row') else row_index
                            end_col = search.group('end_col')
                            end_row = search.group('end_row') if search.group('end_row') else row_index

                            merge_range = f'{start_col}{start_row}:{end_col}{end_row}'
                            worksheet.merge_range(merge_range, text, cell_format)

                            if start_row != end_row:
                                for letter_index in range(alphabet.index(start_col), alphabet.index(end_col) + 1):
                                    for number in range(int(start_row), int(end_row) + 1):
                                        busy_cells.append(f'{alphabet[letter_index]}{number}')

                            col_index = alphabet.index(end_col)
                            if cell.get('width'):
                                worksheet.set_column(f'{alphabet[col_index]}:{alphabet[col_index]}',
                                                     float(cell.get('width')))

                    else:
                        worksheet.write(f'{alphabet[col_index]}{row_index}', text, cell_format)

                        if cell.get('width'):
                            worksheet.set_column(f'{alphabet[col_index]}:{alphabet[col_index]}',
                                                 float(cell.get('width')))

                    col_index += 1
                    del cell_format

                row_index += 1

    workbook.close()
    output.seek(0)

    del tables, text_format

    return output


def generate_excel_from_results(results, title=None, output=None):
    if output is None:
        output = io.BytesIO()
        workbook = xlsxwriter.Workbook(output, {'in_memory': True})
    else:
        workbook = xlsxwriter.Workbook(output)

    alphabet = \
        [letter for letter in string.ascii_uppercase] + \
        [f'A{letter}' for letter in string.ascii_uppercase]

    worksheet_index = 1
    for _, result in results.items():
        worksheet = workbook.add_worksheet(name=result['title'][:30] if result['title'] else f'Лист{worksheet_index}')
        worksheet.set_column(0, 100, 20)
        format = workbook.add_format({'border': 1})
        format.set_text_wrap()

        start_index = 0
        worksheet.set_column(1, 1, 30)

        if title:
            worksheet.merge_range('A1:%s1' % alphabet[len(result['columns']) - 1], title)
            start_index += 1

        for index, name in enumerate(result['columns'], start=0):
            worksheet.write(start_index, index, name, format)

        start_index += 1

        for row_index, line in enumerate(result['lines'], start=start_index):
            for col_index, name in enumerate(result['columns'], start=0):
                worksheet.write(row_index, col_index, line[name], format)

        worksheet_index += 1

    workbook.close()
    output.seek(0)

    return output
